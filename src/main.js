import Vue from "vue";
import VueResource from "vue-resource";
import VueScrollTo from "vue-scrollto";
import App from "./App.vue";
import router from "./router";

Vue.config.productionTip = false;

Vue.use(VueResource);
Vue.use(VueScrollTo);

new Vue({
  router: router,
  http: {
    root: "/public"
  },
  render: h => h(App)
}).$mount("#app");
