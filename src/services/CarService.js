import Vue from "vue";

const DATA_ENDPOINT = "data/cars.json";

export const carService = new Vue({
  name: "carService",
  methods: {
    getAllCars() {
      return this.$http.get(DATA_ENDPOINT);
    }
  }
});
