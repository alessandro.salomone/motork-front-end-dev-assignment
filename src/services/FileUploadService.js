import Vue from "vue";

export const fileUploadService = new Vue({
  name: "fileUploadService",
  methods: {
    upload: function() {
      return new Promise(resolve => setTimeout(resolve, 3000));
    }
  }
});
