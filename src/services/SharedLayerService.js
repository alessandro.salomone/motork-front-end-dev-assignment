import Vue from "vue";

export const sharedLayerService = new Vue({
  name: "sharedLayerService",
  methods: {
    open: function () {
      this.$emit('openSharedLayer');
    },
    close: function () {
      this.$emit('closeSharedLayer');
    },
    startAnimation: function () {
      this.$emit('startSharedLayerAnimation');
    },
    endAnimation: function () {
      this.$emit('endSharedLayerAnimation');
    }
  }
});