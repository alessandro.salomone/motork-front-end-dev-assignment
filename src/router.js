import Vue from "vue";
import Router from "vue-router";
import Gallery from "./views/Gallery";
import Partecipate from "./views/Partecipate";
import Showcase from "./views/Showcase";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/partecipate",
      name: "partecipate",
      component: Partecipate
    },
    {
      path: "/showcase",
      name: "showcase",
      component: Showcase
    },
    {
      path: "/gallery/:page",
      name: "gallery",
      component: Gallery
    },
    {
      path: "/gallery",
      redirect: "/gallery/1"
    },
    {
      path: "*",
      redirect: "/partecipate"
    }
  ]
});
